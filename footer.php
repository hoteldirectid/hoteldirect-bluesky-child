<footer>
	<div class="hdfooter" style="background-color: #1f2021; color: #777;">
		<div class="hdfooter__inner paddtb-60">
			<div class="container">
				<div class="row">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<div class="hdfooterbox">
							<img src="https://hotel.indohotels.id/wp-content/uploads/2017/04/hotels-direct.png" class="img-responsive logo-img" alt="Hotel WP" title="Hotel WP">
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="hdfooterbox">
							<h5>Address</h5>
							<?php if(!empty(ot_get_option('krs_address'))) : ?>
								<p class="hdft__items"><?php echo ot_get_option('krs_address'); ?></p>
							<?php endif; ?>
						</div>
					</div>

					<div class="col-md-3 col-sm-3 col-xs-12">
						<div class="hdfooterbox">
							<h5>Contact</h5>

							<?php if(!empty(ot_get_option('krs_email'))) : ?>
							<p class="hdft__items"><a href="mailto:<?php echo ot_get_option('krs_email'); ?>"><?php echo ot_get_option('krs_email'); ?></a>
							</p>
							<?php endif; ?>

							<?php if(!empty(ot_get_option('krs_phone'))) : ?>
								<p class="hdft__items"><?php echo ot_get_option('krs_phone'); ?></p>
							<?php endif; ?>

							<?php if(!empty(ot_get_option('krs_fax'))) : ?>
								<p class="hdft__items"><?php echo ot_get_option('krs_fax'); ?></p>
							<?php endif; ?>

							<?php if(!empty(ot_get_option('krs_whatsapp'))) : ?>
								<p class="hdft__items"><?php echo ot_get_option('krs_whatsapp'); ?></p>
							<?php endif; ?>

							<?php if(!empty(ot_get_option('krs_bbm'))) : ?>
								<p class="hdft__items"><?php echo ot_get_option('krs_bbm'); ?></p>
							<?php endif; ?>

						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<div class="hdfooterbox hdfooterbox__socmed">
							<h5>Get in touch</h5>
							<?php krs_sn(); ?>
						</div>
					</div>
				</div><!-- end .row -->

				<div class="row">
					<div class="col-md-12">
						<div class="hdfooterlink paddtb-20">
							<nav class="nav text-center nav-bottom">
								<?php karisma_nav_footer(); ?>
							</nav>
						</div>
					</div>
				</div><!-- end .row -->

				<div class="row">
					<div class="col-md-12">
						<p class="tx-center"><?php echo ot_get_option('krs_footcredits'); ?></p>
					</div>
				</div><!-- end .row -->
			</div><!-- end .container -->
		</div><!-- end .hdfooter__inner -->
	</div><!-- end .hdfooter -->
</footer>

<?php wp_footer(); ?>

<?php if(!empty(ot_get_option('krs_map'))) : ?>
<a class="popup-gmaps" href="<?php echo ot_get_option('krs_map'); ?>">
	<span>
		<i class="fas fa-map-marker-alt"></i>
	</span>
</a>
<?php endif; ?>

</body>
</html>
