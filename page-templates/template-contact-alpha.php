<?php

/* Template Name: Contact - Alpha Template */
get_header('image');

?>

<main role="main">
	<div class="hdcontact paddtb-60">
		<div class="container">

			<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="hdcontactbox">
								<h5>Contact Us</h5>
								<div class="hdctbox margibo-20">
									<?php if(!empty(rwmb_meta('contact_address'))) : ?>
									<p class="hdct__items"><?php echo rwmb_meta('contact_address'); ?></p>
									<?php endif; ?>
								</div>

								<div class="hdctbox margibo-20">
									<?php if(!empty(rwmb_meta('contact_phone'))) : ?>
										<p class="hdct__items">
											<?php
												$values = rwmb_meta( 'contact_phone' );
												foreach ( $values as $value )
												{
													echo '<span>'. $value . '</span>';
												}
											?>
											(phone)
										</p>
									<?php endif; ?>
								</div>

								<div class="hdctbox margibo-20">
									<?php if(!empty(rwmb_meta('contact_email'))) : ?>
										<p class="hdct__items">Email :
											<?php
												$values = rwmb_meta( 'contact_email' );
												foreach ( $values as $value )
												{
													echo '<span><a href="mailto:' . $value . '">'. $value .'</a></span>';
												}
											?>
										</p>
									<?php endif; ?>
								</div>

							</div><!-- end .hdcontactbox -->
						</div>

						<div class="col-md-8 col-sm-8 col-xs-12">
							<div class="hdcontactbox hdcontactbox__ctform">
								<?php the_content(); ?>
							</div><!-- end .hdcontactbox -->
						</div>
					</div><!-- end .row -->
				</article>

				<div class="hdcontactbox hdcontactbox__ctmap">
					<?php echo rwmb_meta('map'); ?>
				</div>

			<?php endwhile; ?>

			<?php else: ?>
				<article>
					<h2 class="title text-center"><?php _e( 'Sorry, nothing to display.', karisma_text_domain ); ?></h2>
				</article>
			<?php endif; ?>

		</div><!-- end .container -->
	</div><!-- end .hdcontact -->
</main>

<?php get_footer(); ?>
