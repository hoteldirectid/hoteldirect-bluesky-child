<?php

/* Template Name: Facilities - Alpha Template */

get_header('image');

?>

<main role="main">
	<div class="hduni">
		<div class="container">

			<h1 class="title text-center"><?php the_title(); ?></h1>

			<div class="row">

				<?php
					$args = array(
						'post_type' => 'hotel-info',
						'category_name' => 'facilities',
					);
					query_posts($args);
					if (have_posts()): while (have_posts()) : the_post();
				?>

				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="hdunibox hdunibox__wo">
						<div class="hdunipic">
							<span>
								<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
									<img src="<?php the_post_thumbnail_url('gallery-slide'); ?>" alt="<?php the_title_attribute(); ?>">
								<?php endif; ?>
							</span>
						</div>
						<div class="hduniname">
							<h4><?php the_title(); ?></h4>
						</div><!-- end .hduniname -->
						<span class="hdoverlay"></span>
					</div><!-- end .hdunibox -->
				</div>

				<?php endwhile; ?>
				<?php else: ?>

				<h2><?php _e( 'Sorry, nothing to display.', karisma_text_domain ); ?></h2>

				<?php endif; ?>
				<?php get_template_part('pagination'); ?>

			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>
